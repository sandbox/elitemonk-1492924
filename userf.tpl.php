<?php

/**
 * @file
 * Default theme implementation to display a User Field.
 *
 * Available variables:
 * - $uid: The field user's ID.
 * - $name: Themed username of field user output from theme_username().
 * - $picture: The field user's picture from user-picture.tpl.php.
 */
?>
<div id="userf-<?php print $uid; ?>" class="userf">

  <div class="userf-picture">

    <?php print $picture; ?>

  </div>

  <div class="userf-name">

    <strong>
      <?php print $name; ?>
    </strong>

  </div>

</div>

